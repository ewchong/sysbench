#!/bin/bash

usage() {
	echo "$0 [-c] <workload name> <workload tool options>" 1>&2
	echo " -c Run in a container, otherwise default to baremetal" 1>&2
	exit 1;
}

# Default to sysbench and baremetal
CMD="sysbench" 
ENVIRONMENT="baremetal"

DATE=$(date -u +"%Y-%m-%d")

while getopts "c" o; do
	case "${o}" in
		c)
			CMD="podman run -it edchong/sysbench"
			ENVIRONMENT="container"
			;;
		*)
			usage
			;;
	esac
done
shift $((OPTIND-1))
		
if [ "$#" -lt 2 ]; then
   usage
fi

WORKLOAD=$1
FILENAME=$(hostname)_sysbench_${WORKLOAD}_${ENVIRONMENT}_${DATE}

# Start atop[
atop 1 -w $FILENAME.atop &

START_TIME=$(date)
$CMD $@ | tee -a ./${FILENAME}.log
END_TIME=$(date)

# Sleep 5 seconds to have system calm down
sleep 5

# Stop atop
pkill atop


echo "start_time: ${START_TIME}" >> $FILENAME.log
echo "end_time: ${END_TIME}" >> $FILENAME.log
echo "atop $(atop -V)" >> $FILENAME.log
echo "workload_args: $@" >> $FILENAME.log
echo "===============================================" >> $FILENAME.log
