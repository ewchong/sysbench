# sysbench-arm-container

Create a container for ARM based on `arm64v8/centos` on a Raspberry PI 4

## Building

```
podman build -t registry.gitlab.com/ewchong/sysbench .
```

## Using

```
podman run registry.gitlab.com/ewchong/sysbench <SYSBENCH ARGS>
```



